import os

# imgur_id = os.environ['imgur_id']

reddit_client_id = os.environ['reddit_client_id']
reddit_client_secret = os.environ['reddit_client_secret']
reddit_user = os.environ['reddit_user']
reddit_password = os.environ['reddit_password']

streamable_pass = os.environ['streamable_pass']
streamable_user = os.environ['streamable_user']

pfycat_client_id = os.environ['pfycat_client_id']
pfycat_client_secret = os.environ['pfycat_client_secret']
pfycat_username = os.environ['pfycat_username']
pfycat_password = os.environ['pfycat_password']
